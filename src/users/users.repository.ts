import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { User } from './user.entity';

/**
 * Допишите оставшиеся методы учитывая пример с findAll
 */
class UsersRepository implements EntityRepository<User> {
  async findAll(): Promise<User[]> {
    try {
      /**
       * Соединение с БД лежит в обычной переменной
       * Чтобы достать репозиторий для сущности вызываю метод getRepository
       * Далее работаю уже с методами репозитория
       * Потыкайте все методы репозитория, их названия очень интуитивные
       * find, findOne, save, delete, update и проч
       */
      return dbConnection.getRepository(User).find();
    } catch (e) {
      throw e;
    }
  }
}

export const usersRepository = new UsersRepository();
